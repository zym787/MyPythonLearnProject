opt_strs = [
    ["查询列车信息", "管理员登录 ", "用户登录  ", "用户注册  ", "退出系统  "],
    ["查询列车信息", "添加列车信息", "删除列车信息", "修改列车信息", "用 户 解 锁", " 退    出 "],
    ["查询列车信息", " 购    票 ", " 退    票 ", " 退    出 "]
    ]
welc_array = [
    "中文简体:         欢迎使用--- 🐂小黄牛 ---火车票售票系统 V1.0",
    "中文繁體:         歡迎使用--- 🐂小黃牛 ---火車票售票系統 V1.0",
    "русск:           Добро пожаловать--- 🐂Желторотый ---Система билетов на поезд V1.0",
    "English:         Welcome to use--- 🐂Little Scalper ---Train ticket sales system V1.0",
    "日本語:           ご利用歓迎--- 🐂小牡牛 ---列車の切符の販売システムV1.0",
    "Das ist Deutsch: Willkommen an bord--- 🐂Schwarzblut ---Reisetickets: automatensystem V1.0"
]

screen_width = 100
text_width = len(welc_array[0])
for i in range(0,6):
    if text_width <= len(welc_array[i]):
        text_width = len(welc_array[i])
    else:
        text_width = text_width

text_width = text_width + 3
box_width:98
left_margin:1
print("text_width:{},box_width:{},left_margin:{}".format(text_width,box_width,left_margin))

screen_width = 100
text_width = 92
box_width = text_width + 9
left_margin = (screen_width - box_width) // 2

l_margin = [1, 1, 1, 1, 1, 1]
r_margin = [38, 38, 9, 6, 29, 1]
print(' ' * left_margin + '+' + '-' * (box_width) + '+')
print(' ' * (left_margin + 2) + '| ' + ' ' * text_width + ' |')
print(' ' * (left_margin + 2) + '| ' + ' ' * l_margin[0] + welc_array[0] + ' ' * r_margin[0] + ' |')
print(' ' * (left_margin + 2) + '| ' + ' ' * l_margin[1] + welc_array[1] + ' ' * r_margin[1] + ' |')
print(' ' * (left_margin + 2) + '| ' + ' ' * l_margin[2] + welc_array[2] + ' ' * r_margin[2] + ' |')
print(' ' * (left_margin + 2) + '| ' + ' ' * l_margin[3] + welc_array[3] + ' ' * r_margin[3] + ' |')
print(' ' * (left_margin + 2) + '| ' + ' ' * l_margin[4] + welc_array[4] + ' ' * r_margin[4] + ' |')
print(' ' * (left_margin + 2) + '| ' + ' ' * l_margin[5] + welc_array[5] + ' ' * r_margin[5] + ' |')
print(' ' * (left_margin + 2) + '| ' + ' ' * text_width + ' |')
print(' ' * left_margin + '+' + '-' * (box_width) + '+')

input("Press Enter to Exit")
