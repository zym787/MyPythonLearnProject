print(" 删除列车信息".rjust(27, '^'))
print(" 要删除车次信息请直接输入所要删除的车次 \n 输入'退出'或'quit'或'q'退出查询")
in_temp = input(" 请输入想要的操作:".rjust(30, '^')).strip()
if 'K' == in_temp[0] or 'D' == in_temp[0] or 'G' == in_temp[0] or 'T' == in_temp[0] or \
        'k' == in_temp[0] or 'd' == in_temp[0] or 'g' == in_temp[0] or 't' == in_temp[0]:
    with open('test.csv', mode='r+', encoding='gbk') as f:
        train = f.readlines()
        train_row_temp = ""
        train_flag = False
        for train_row in train:  # 遍历文件 车次查重
            if in_temp in train_row:
                train_flag = False
                # 在该行找到
                # f.writelines("")
                train_row = ""
            else:
                train_flag = True
                # 在该行未找到
                train_row_temp += train_row
                # print(train_row_temp)
                # f.writelines(train_row)
                # f.writelines("")
        print(train_row_temp)
        print(train_flag)
        # 判断是否可以删除该车次
        if train_flag:  # 可删除车次标志
            train_flag = False
            with open('test.csv', mode='r+', encoding='gbk') as w:
                w.truncate()
                w.write(train_row_temp)
                print(train_row_temp)
                w.close()
            print("删除成功！")
        else:
            print("此列车未找到，无法删除")
else:
    print("输入的不是车次，请检查！")
