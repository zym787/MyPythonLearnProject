# 提取字母字符串
import csv
import re


# string = "K123"
# result = ''.join(re.findall(r'[A-Za-z]', string))
# print(result)

# 列车信息
train_info_added = \
    ["车次", "始发站", "终点站", "票价(元)", "票数(张)"]


# 列车车次信息解析
def train_info_analyse(strs):
    result = ''.join(re.findall(r'[A-Za-z]', strs))
    if 'K' == result or 'k' == result:
        return '快车'
    elif 'D' == result or 'd' == result:
        return '动车'
    elif 'Z' == result or 'z' == result:
        return '直达'
    elif 'T' == result or 't' == result:
        return '特快'


# 添加列车信息
def train_info_add():
    write_flag = False
    with open('trainInfomation.csv', mode='r+', encoding='gbk') as f:
        train = f.readlines()
        in_info_str = str(input("请输入" + train_info_added[0] + ":"))
        for row in train:   # 遍历文件 车次查重
            if in_info_str in row:
                write_flag = False
                break
            else:
                # 置可添加车次标志
                write_flag = True
        # 判断是否可以添加该车次
        if write_flag:  # 可添加车次标志
            write_flag = False
            f.write(in_info_str[:10])
            for i in range(len(train_info_added)-1):  # 输入列车信息
                in_info_str = str(input("请输入" + train_info_added[i+1] + ":"))
                f.write("," + in_info_str[:10])
            print("车次不存在，已添加")
            f.write("\n")
            f.close()
            print("文件已关闭")
        else:
            print("该车次已存在！请检查后重新输入")
    return True


print("+" + "这是分割线".center(98, "-") + "+")
train_info_add()
print("+" + "这是分割线".center(98, "-") + "+")
# with open('trainInfomation.csv', 'r', encoding='gbk') as f:  # trainInfomation.csv 为Data目录下的文件
#     train_csv = csv.reader(f)
#     temp = ['None']
#     # 输出列车信息
#     for row in train_csv:  # 文件遍历 -- 以行的形式
#         for i in range(len(row)):  # 行遍历 -- 以每一个数据
#             if 0 == i:
#                 result = ''.join(re.findall(r'[A-Za-z]', row[0]))
#                 row.insert(1, train_info_analyse(result))
#         print(row)
#         print()
