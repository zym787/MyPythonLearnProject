# 1.登录成功显示欢迎页面
# 2.登录失败显示密码错误，并显示错误几次
# 3.登录三次失败后，退出程序
# 1.可以支持多个用户登录 
# 2.用户3次认证失败后，退出程序，再次启动程序尝试登录时，还是锁定状态
# 3.可以创建用户
# 4.可以删除用户
# 5.可以修改用户
print('欢迎来到用户登陆程序')
print('1.创建用户')
print('2.登陆用户')
print('3.删除用户')
print('4.修改用户')

print('=' * 100)
choose = input('请选择你的操作:')
# 创建用户
if choose == '1':
    useradd = input('请输入你要创建的唯一用户：')
    passwdad = input('请输入你设置6位字母数字组合的密码：')
    with open('../item_TrainTicketSalesSystem/a', 'r', encoding='gbk') as f:  # a 为当前目录文件
        user = f.read()
    if passwdad.isalnum() and len(passwdad) == 6 and useradd not in user:
        with open('../item_TrainTicketSalesSystem/a', 'a', encoding='gbk') as f:
            f.write(useradd + passwdad + '\n')
# 登陆用户
if choose == '2':
    j = 0
    while True:
        with open('../item_TrainTicketSalesSystem/a', 'r', encoding='gbk') as f:
            user = f.read()
        with open('../item_TrainTicketSalesSystem/b', 'r', encoding='gbk') as f:
            nouser = f.read()
        user1 = input('请输入用户：')
        if user1 in nouser:
            print('该用户已锁定')
            break
        if user1 not in user:
            print('该用户不存在')
            break
        passwd1 = input('请输入密码：')
        if len(passwd1) != 6:
            print('你二大爷的让你输入6位密码')
            break
        if user1 + passwd1 in user:
            print('欢迎登陆')
            break
        if passwd1 not in user:
            n = j + 1
            print('密码输入错误次数:')
            print(n)
            j += 1
            if j == 3:
                print('该用户已锁定')
                with open('../item_TrainTicketSalesSystem/b', 'a', encoding='gbk') as f:  # b为当前目录文件
                    f.write(user1)
                break
# 删除用户
if choose == '3':
    userdel = input('请输入你要删除的用户名')
    passwd = input('请输入你要删除的用户密码')
    with open(r'../item_TrainTicketSalesSystem/a', 'r', encoding='gbk') as f:
        l1 = f.readlines()
    with open(r'../item_TrainTicketSalesSystem/a', 'w', encoding='gbk') as w:
        for l in l1:
            if userdel + passwd not in l:
                w.write(l)
# 修改用户名
if choose == 4:
    useradd1 = input('请输入你要修改的用户名：')
    passwd1 = input('请输入你要改类型的密码：')
    useradd2 = input('请输入你要修改的用户名：')
    passwd2 = input('请输入你要修改的用户密码：')
    i = useradd1 + passwd1
    j = useradd2 + passwd2
    with open(r'../item_TrainTicketSalesSystem/a', 'r', encoding='gbk') as f:
        l1 = f.readlines()
    with open(r'../item_TrainTicketSalesSystem/a', 'w', encoding='gbk') as w:
        for l in l1:
            if i not in l:
                w.write(l)
            else:
                w.write(j)