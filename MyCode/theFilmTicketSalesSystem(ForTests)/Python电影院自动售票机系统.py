import re  # 判断所选座位(如1-1)时需要用到正则表达式


class ATM:
    """自动售票机"""
    data = {'2020-5-5': [  # 今日售票信息 已知信息 八个电影各三个场次各100票
        {'紧急救援': {'9:30': list(range(1, 101)), '10:40': list(range(1, 101)), '14:30': list(range(1, 101))}},
        {'急先锋': {'10:30': list(range(1, 101)), '11:40': list(range(1, 101)), '15:30': list(range(1, 101))}},
        {'花木兰': {'8:30': list(range(1, 101)), '10:40': list(range(1, 101)), '14:30': list(range(1, 101))}},
        {'我和我的祖国': {'10:30': list(range(1, 101)), '11:40': list(range(1, 101)), '15:30': list(range(1, 101))}},
        {'千与千寻': {'10:30': list(range(1, 101)), '12:40': list(range(1, 101)), '15:30': list(range(1, 101))}},
        {'大鱼海棠': {'8:30': list(range(1, 101)), '11:40': list(range(1, 101)), '15:30': list(range(1, 101))}},
        {'十二生肖2': {'9:30': list(range(1, 101)), '11:40': list(range(1, 101)), '15:30': list(range(1, 101))}},
        {'最后的魁拔': {'10:30': list(range(1, 101)), '14:40': list(range(1, 101)), '18:30': list(range(1, 101))}}]}
    today = '2020-5-5'  # 今天日期 已知信息
    user = ""  # 用户名  root
    password = ""  # 密码  123
    is_login = False  # 用户是否登陆
    is_func = True  # 系统是否运行
    option = ""  # 所选电影名称  如：红海行动(3)
    option_key = ""  # 所选电影索引  则：2
    scene = ""  # 所选场次  如：8:30(1)
    scene_key = ""  # 所选场次索引  则：0
    seat = ""  # 所选座位  如：1-1(1)
    seat_key = ""  # 所选座位索引  则：0
    seat_value = ""  # 所选座位索引值(票号)  则：1

    def __init__(self):  # __init__()方法
        self.loading()  # 加载函数

    def loading(self):  # 系统加载函数
        is_func = input("Loading(输入任意字符激活)...\n")
        if is_func == "exit":  # 退出系统
            self.is_func = False
            print("正在退出系统...")

    def login(self):  # 用户登陆函数
        self.user = input("请输入用户名(root)：")
        self.password = input("请输入密码(123)：")
        if self.user == "root" and self.password == "123":  # 默认有一个root用户
            self.is_login = True
            print("已成功登陆！开始选票！\n")
        else:
            print("密码或用户名输入错误！")
            self.login()

    def format_tip_option(self):  # 电影选择函数
        tip_option = ""  # 电影提示信息
        for i in range(len(self.data[self.today])):  # 初始化电影提示信息
            data_option = list(self.data[self.today][i])
            tip_option = tip_option + str(i + 1) + "、《" + data_option[0] + "》 "
        tip_option = "请选择正在上映的电影：" + tip_option + '\n'
        self.option = input(tip_option)  # 所选电影名称
        self.option_key = ""  # 所选电影索引
        if str.isdigit(self.option) and 0 < int(self.option) <= len(self.data[self.today]):  # 输入内容为电影提示信息序列
            self.option_key = int(self.option) - 1  # 所选电影索引
            self.option = list(self.data[self.today][self.option_key])[0]  # 所选电影名称
        elif str.isalpha(self.option) and u'\u4e00' <= self.option <= u'\u9fa5':  # 输入内容为电影名称
            for i in range(len(self.data[self.today])):
                data_option = list(self.data[self.today][i])
                if self.option == data_option[0]:
                    self.option_key = i  # 所选电影索引
        if self.option_key == "":  # 没有找到所选电影索引
            print("您选择错误！请重新选择！")
            self.format_tip_option()  # 电影选择函数

    def format_tip_scene(self):  # 场次选择函数
        print("已选电影：《" + self.option + "》\n")
        tip_scene = ""  # 场次信息
        data_scene = list(self.data[self.today][self.option_key].values())  # 初始化场次信息
        data_option_scene = list(data_scene[0])
        for i in range(len(data_option_scene)):
            tip_scene = tip_scene + str(i + 1) + "、" + data_option_scene[i] + " "
        tip_scene = "请选择电影播放场次：" + tip_scene + "\n"
        self.scene = input(tip_scene)  # 选择场次
        if str.isdigit(self.scene) and 0 < int(self.scene) <= len(data_option_scene):  # 输入内容为数字
            self.scene_key = int(self.scene) - 1
            self.scene = data_option_scene[self.scene_key]
        else:
            for i in range(len(data_option_scene)):
                if self.scene == data_option_scene[i]:
                    self.scene_key = i
        if self.scene_key == "":
            print("您选择错误！请重新选择！")
            self.format_tip_scene()  # 场次选择函数

    def format_tip_seat(self):
        print("已选电影场次：" + self.scene + '\n')
        tip_seat = ""  # 初始化座位信息
        data_seat = list(list(self.data[self.today][self.option_key].values())[0].values())[self.scene_key]
        for i in range(len(data_seat)):  # 默认每个场次座位有10列
            had_format = '{:^3}'  # 已选择
            x_format = '{:>2}'  # 格式化座位-x(行)
            y_format = '{:<2}'  # 格式化座位-y(列)
            if (i + 1) % 10 != 0:
                if str.isdigit(str(data_seat[i])):  # 座位索引值为票号
                    x = data_seat[i] // 10 + 1  # 座位-x(行)
                    y = data_seat[i] % 10  # 座位-y(列)
                    if x == 0:
                        x = 10
                        y = y - 1
                    if y == 0:
                        x = x - 1
                        y = 10
                    tip_seat = tip_seat + x_format.format(str(x)) + "-" + y_format.format(str(y)) + "\t"
                else:  # 座位索引值为用户名
                    tip_seat = tip_seat + had_format.format('已选择') + "\t"
            else:
                if str.isdigit(str(data_seat[i])):  # 座位索引值为票号
                    x = data_seat[i] // 10 + 1  # 座位-x(行)
                    y = data_seat[i] % 10  # 座位-y(列)
                    if x == 0:
                        x = 10
                        y = y - 1
                    if y == 0:
                        x = x - 1
                        y = 10
                    tip_seat = tip_seat + x_format.format(str(x)) + "-" + y_format.format(str(y)) + "\n\t"
                else:  # 座位索引值为用户名
                    tip_seat = tip_seat + had_format.format('已选择') + "\n\t"
        self.seat = input("请选择座位：\n\t%s" % tip_seat)
        if str.isdigit(self.seat):  # 输入内容为座位索引值
            self.seat_value = int(self.seat)  # 所选座位索引值
            x = self.seat_value // 10 + 1  # 座位-x(行)
            y = self.seat_value % 10  # 座位-y(列)
            if x == 0:
                x = 10
                y = y - 1
            if y == 0:
                x = x - 1
                y = 10
            self.seat = str(x) + '-' + str(y)  # 所选座位
        elif re.match(r'[0-9]+-+[0-9]+$', self.seat, re.X):  # 输入内容为座位(如：1-1 1-10 10-1 10-10)
            x = re.findall(r'[0-9]', self.seat)[0]  # 座位-x(行)
            y = re.findall(r'[0-9]', self.seat)[1]  # 座位-y(列)
            self.seat_value = (int(x) - 1) * 10 + int(y)  # 所选座位索引值
        if self.seat_value not in self.data[self.today][self.option_key][self.option][self.scene]:  # 没有找到所选座位索引
            print("您选择错误！请重新选择！")
            self.format_tip_seat()  # 座位选择函数

    def data_update(self):  # 数据更新
        self.seat_key = self.data[self.today][self.option_key][self.option][self.scene].index(self.seat_value)
        print("已选座位：" + self.seat + '\n')
        print('正在出票...')
        self.data[self.today][self.option_key][self.option][self.scene][self.seat_key] = self.user  # 将所选座位改为用户名

    def data_option(self):  # 出票
        print('''
电影：《%s》
播出时间：%s %s
座位：%d号(%s)

出票完成，请别忘记取票
''' % (self.option, self.today, self.scene, self.seat_value, self.seat))

    def main(self):  # 主函数
        print("欢迎使用自动售票机\n")
        self.login()  # 用户登陆
        self.format_tip_option()  # 选择电影
        self.format_tip_scene()  # 选择场次
        self.format_tip_seat()  # 选择座位
        self.data_update()  # 更新数据
        self.data_option()  # 出票信息


app = ATM()  # 实例化ATM对象
while app.is_func:  # 系统是否运行
    app.main()  # app主函数
    app.loading()  # app加载函数
