# commonFile.py
import csv
import os
import random

# 界面信息
import sys
import time

opt_strs = [
    ["查询列车信息", "管理员登录 ", "用户登录  ", "用户注册  ", "退出系统  "],
    ["查询列车信息", "添加列车信息", "删除列车信息", "修改列车信息", "用户解锁  ", "返回上一级 "],
    ["查询列车信息", "用户购票  ", "用户退票  ", "返回上一级 "]
]
# 多语种欢迎语
welc_array = [
    "中文简体:         欢迎使用--- 🐂小黄牛 ---火车票售票系统 V1.0",
    "中文繁體:         歡迎使用--- 🐂小黃牛 ---火車票售票系統 V1.0",
    "русск:           Добро пожаловать--- 🐂Желторотый ---Система билетов на поезд V1.0",
    "English:         Welcome to use--- 🐂Little Scalper ---Train ticket sales system V1.0",
    "日本語:           ご利用歓迎--- 🐂小牡牛 ---列車の切符の販売システムV1.0",
    "Das ist Deutsch: Willkommen an bord--- 🐂Schwarzblut ---Reisetickets: automatensystem V1.0"
]


train_info = ["车次", "始发站", "终点站", "票价(元)", "票数(张)"]


# 初始化
def system_init():
    if not os.path.isfile("./Data/trainInfomation.csv") \
            or not os.path.isfile("./Data/userDataBase.csv") \
            or not os.path.isfile("./Data/userBlacklist.csv"):
        try:
            with open('.\\Data\\userDataBase.csv', 'r', encoding='gbk') as f:
                f.close()
        except IOError:
            with open('.\\Data\\userDataBase.csv', 'w', encoding='gbk') as f:
                f.close()
        try:
            with open('.\\Data\\userBlacklist.csv', 'r', encoding='gbk') as f:
                f.close()
        except IOError:
            with open('.\\Data\\userBlacklist.csv', 'w', encoding='gbk') as f:
                f.close()
        try:
            with open('.\\Data\\trainInfomation.csv', 'r', encoding='gbk') as f:
                f.close()
        except IOError:
            with open('.\\Data\\trainInfomation.csv', 'w', encoding='gbk') as f:
                f.close()
    print("current working directory", os.getcwd())
    print(os.path.exists(".\\Data\\trainInfomation.csv"))
    print(os.path.exists(".\\Data\\userDataBase.csv"))
    print(os.path.exists(".\\Data\\trainInfomation.csv"))

    # 进度条
    for i in range(1, 101):
        print("\r", end="")
        print("Loading progress: {}%: ".format(i), "▋" * (i // 2), end="")
        sys.stdout.flush()
        time.sleep(0.05)
    print()

    print("+" + "初始化完成".center(98, "-") + "+")
    print("+" + "这是分割线".center(98, "-") + "+")



# 选项函数
def opt_menu(opt_strs):
    for i in range(len(opt_strs)):
        print("{} {}：{} {}".format("", i + 1, opt_strs[i], "").center(50, '*'))
    opt_str = input("{} 请输入操作编号 1~{}：".format("", len(opt_strs)).rjust(33, '~'))
    return opt_str


# 生成验证码
def captcha():
    n = 6  # 定义验证码位数，以提高安全性
    print("+" + "这是分割线".center(98, "-") + "+")
    print("+" + "请输入验证码".center(98, "-") + "+")
    while True:
        check = random.randint(10 ** (n-1), 10 ** n - 1)  # 生成n位随机验证码
        in_check = input(" 请输入'exit'退出或输入验证码({})：".format(check).rjust(34, '~'))
        if in_check == 'exit':
            return False
        try:
            in_check = int(in_check)
            if isinstance(in_check, int) and n == len(str(in_check)):
                if check == in_check:
                    return True
                else:
                    print(" 验证码错误！".rjust(27, '~'))
                    return False
        except Exception as e:
            print(" 别挣扎了~伙计，试着输入一些数字吧".rjust(36, '~'))
            continue


# 查询列车信息
def train_query():
    train_flag = False
    while True:
        print(" 查询列车信息".rjust(27, '^'))
        print(" 要查询车次信息请直接输入所要查询的车次或地点 \n 输入'查询'或'all'输出所有列车信息 \n 输入'quit'退出查询")
        in_temp = input(" 请输入想要的操作:".rjust(30, '^')).strip()
        if in_temp == "查询" or in_temp == "all":
            print(" 输出列车信息：".rjust(28, '^'))
            # 读取列车信息数据库
            with open('../item_TrainTicketSalesSystem/Data/trainInfomation.csv', 'r', encoding='gbk') as f:  # trainInfomation.csv 为当前目录文件
                train_csv = csv.reader(f)
                # 输出表头
                for i in range(len(train_info)):
                    print(train_info[i].ljust(11, ' ') + "  ", end="")
                print()
                # 输出列车信息
                for row in train_csv:
                    for i in range(len(row)):
                        print(row[i].ljust(11, ' ') + "   ", end="")
                    print()
        elif in_temp == "quit":
            break
        else:
            # 查询输入的列车信息
            with open('../item_TrainTicketSalesSystem/Data/trainInfomation.csv', 'r', encoding='gbk') as f:  # trainInfomation.csv 为当前目录文件
                train_csv = csv.reader(f)
                # 输出表头
                for i in range(len(train_info)):
                    print(train_info[i].ljust(11, ' ') + "  ", end="")
                print()
                for row in train_csv:
                    if in_temp in row:
                        for i in range(len(row)):
                            print(row[i].ljust(11, ' ') + "   ", end="")
                        print()
                        train_flag = True
                    else:
                        train_flag = False
            if not train_flag:
                print("此列车信息未找到")
    return True

