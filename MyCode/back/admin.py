# admin.py
from commonFile import *


# 添加列车信息
def train_info_add():
    write_flag = False
    with open('../item_TrainTicketSalesSystem/Data/trainInfomation.csv', mode='a+', encoding='gbk') as f:
        train = f.readlines()
        for i in range(len(train_info)):
            in_info_str = str(input("请输入" + train_info[i] + ":"))
            if i == 0:  # 检查车次是否重复
                for cache in train:  # 提取列车信息文件中的车次信息
                    cache = cache.split()
                    if in_info_str == cache[0]:  # 检查车次是否存在
                        print("该车次已存在！请检查后重新输入")
                        write_flag = True
                        break
                    else:
                        print("车次不存在，已添加")
                        write_flag = True
                        # 这里因为中国最长的地名为：那然色布斯台音布拉格，10个字符，故对输入的数据进行限长处理
                        f.write(in_info_str[:10].ljust(11, ' ') + " ")
                        continue
        if write_flag:
            f.write("\n")
            write_flag = False
        f.close()
    return True


# 删除列车信息
def train_info_del():
    print("删除列车信息")
    return True


# 修改列车信息
def train_info_change():
    print("修改列车信息")
    return True


# 用户解锁
def user_unlock():
    with open(r'userBlacklist.csv', 'r', encoding='gbk') as f:
        l1 = f.readlines()
    print(" 被锁定的用户:".rjust(28, '~'), l1)
    im_name = input(" 请输入你要解锁的用户名:".rjust(33, '~'))
    with open(r'userBlacklist.csv', 'r', encoding='gbk') as f:
        l1 = f.readlines()
    with open(r'userBlacklist.csv', 'w', encoding='gbk') as w:
        for l in l1:
            if im_name not in l:
                w.write(l)
            else:
                w.write("")
    return True


# 管理员登录
def admin_logon():
    if captcha():
        in_name = input(" 请管理员输入用户名：".rjust(31, '~'))
        in_passwd = input(" 请管理员输入密码：".rjust(30, '~'))
        if in_name == "root" and in_passwd == "0":
            print(" 登录成功，欢迎帅气的管理员".rjust(34, '~'))
            return True
        else:
            print(" 用户名或者密码错误！请重新输入".rjust(36, '~'))
            return False
    else:
        return False


# 管理员界面
def admin_menu(opt_strs):
    while True:
        opt_str = opt_menu(opt_strs)
        # 1 查询列车信息
        if opt_str == '1':
            train_query()
        # 2 添加列车信息
        elif opt_str == '2':
            train_info_add()
        # 3 删除列车信息
        elif opt_str == '3':
            train_info_del()
        # 4 修改列车信息
        elif opt_str == '4':
            train_info_change()
        # 5 用户解锁
        elif opt_str == '5':
            user_unlock()
        # 6 退出
        elif opt_str == '6':
            print("退出...".rjust(26, '~'))
            return True
        else:
            print("输入错误，请重新输入!!!".rjust(34, '~'))
