# commonFile.py
import csv
import os
import random
import re
from datetime import datetime
import time

# 界面信息
import sys

opt_strs = [
    ["查询列车信息", "管理员登录 ", "用户登录  ", "用户注册  ", "退出系统  "],
    ["查询列车信息", "添加列车信息", "删除列车信息", "修改列车信息", "用户解锁  ", "返回上一级 "],
    ["查询列车信息", "用户购票  ", "用户退票  ", "返回上一级 "]
]
# 多语种欢迎语
welc_array = [
    "中文简体:         欢迎使用--- 🐂小黄牛 ---火车票售票系统 V1.0",
    "中文繁體:         歡迎使用--- 🐂小黃牛 ---火車票售票系統 V1.0",
    "русск:           Добро пожаловать--- 🐂Желторотый ---Система билетов на поезд V1.0",
    "English:         Welcome to use--- 🐂Little Scalper ---Train ticket sales system V1.0",
    "日本語:           ご利用歓迎--- 🐂小牡牛 ---列車の切符の販売システムV1.0",
    "Das ist Deutsch: Willkommen an bord--- 🐂Schwarzblut ---Reisetickets: automatensystem V1.0"
]

# 列车信息
train_info_added = \
    ["车次", "始发站", "终点站", "票价(元)", "票数(张)"]
train_info_display = \
    ["车次", "车次类型", "始发站", "终点站", "票价(元)", "票数(张)"]


# 初始化
def system_init():
    # 判断几个数据库是否存在
    if not os.path.isfile("Data/trainInfomation.csv") \
            or not os.path.isfile(".\\Data\\userDataBase.csv") \
            or not os.path.isfile(".\\Data\\userBlacklist.csv"):
        try:
            with open('.\\Data\\userDataBase.csv', 'r', encoding='gbk') as f:
                f.close()
        except IOError:
            with open('.\\Data\\userDataBase.csv', 'w', encoding='gbk') as f:
                f.close()
        try:
            with open('.\\Data\\userBlacklist.csv', 'r', encoding='gbk') as f:
                f.close()
        except IOError:
            with open('.\\Data\\userBlacklist.csv', 'w', encoding='gbk') as f:
                f.close()
        try:
            with open('Data/trainInfomation.csv', 'r', encoding='gbk') as f:
                f.close()
        except IOError:
            with open('Data/trainInfomation.csv', 'w', encoding='gbk') as f:
                f.close()
    print("当前工作目录为:", os.getcwd())
    print("trainInfomation.csv文件", os.path.exists("Data/trainInfomation.csv"))
    print("userDataBase.csv文件", os.path.exists(".\\Data\\userDataBase.csv"))
    print("userBlacklist.csv文件", os.path.exists(".\\Data\\userBlacklist.csv"))
    # 进度条
    for i in range(1, 101):
        print("\r", end="")
        print("Loading progress: {}%: ".format(i), "▋" * (i // 2), end="")
        sys.stdout.flush()
        time.sleep(0.05)
    print()
    print("+" + "初始化完成".center(98, "-") + "+")
    print("+" + "这是分割线".center(98, "-") + "+")


# 时间显示
def time_display(opt_time):
    local_time = datetime.now()  # 创建一个datetime类对象
    time_info = local_time.strftime('%Y-%m-%d %a %I:%M:%S %p')  # 时间信息
    week_info = local_time.strftime('%U')  # 周数信息
    if 'all' == opt_time:
        print("+" + "这是分割线".center(98, "-") + "+")
        print("+" + '当前时间为: {}'.format(time_info).center(98, ' ') + "+")
        print("+" + '本周是今年的第{}周 :)   加油，打工人! '.format(week_info).center(92, ' ') + "+")
        print("+" + "欢迎使用本系统！".center(96, " ") + "+")
        print("+" + "这是分割线".center(98, "-") + "+")
    elif 'time' == opt_time:
        print("+" + " 时间显示 ".center(98, "-") + "+")
        print("+" + '当前时间为: {}'.format(time_info).center(98, ' ') + "+")
        print("+" + " 时间显示 ".center(98, "-") + "+")


# 选项函数
def opt_menu(opt_strs):
    for i in range(len(opt_strs)):
        print("{} {}：{} {}".format("", i + 1, opt_strs[i], "").center(50, '*'))
    opt_str = input("{} 请输入操作编号 1~{}：".format("", len(opt_strs)).rjust(33, '~'))
    return opt_str


# 生成验证码
def captcha():
    n = 4  # 定义验证码位数，以提高安全性
    print("+" + "这是分割线".center(98, "-") + "+")
    print("+" + "请输入验证码".center(98, "-") + "+")
    while True:
        check = random.randint(10 ** (n - 1), 10 ** n - 1)  # 生成n位随机验证码
        in_check = input(" 请输入'exit'退出或输入验证码({})：".format(check).rjust(34, '~'))
        if in_check == 'exit':
            return False
        try:
            in_check = int(in_check)
            if isinstance(in_check, int) and n == len(str(in_check)):
                if check == in_check:
                    return True
                else:
                    print(" 验证码错误！".rjust(27, '~'))
                    return False
        except Exception as e:
            print(" 别挣扎了~伙计，试着输入一些数字吧".rjust(36, '~'))
            continue


# 列车车次信息解析
def train_info_analyse(strs):
    result = ''.join(re.findall(r'[A-Za-z]', strs))
    if 'K' == result or 'k' == result:
        return '快车'
    elif 'D' == result or 'd' == result:
        return '动车'
    elif 'Z' == result or 'z' == result:
        return '直达'
    elif 'T' == result or 't' == result:
        return '特快'
    elif 'G' == result or 'g' == result:
        return '高铁'


# 列车信息输出
def train_info_output():
    # 读取列车信息数据库
    with open('Data/trainInfomation.csv', 'r+', encoding='gbk') as f:  # trainInfomation.csv 为Data目录下的文件
        train_csv = csv.reader(f)
        # 输出表头
        for i in range(len(train_info_display)):
            print(train_info_display[i].ljust(11, ' ') + "  ", end="")
        print()
        # 输出列车信息
        for row in train_csv:  # 文件遍历 -- 以行的形式
            for i in range(len(row) + 1):  # 行遍历 -- 以每一个数据
                if 0 == i:
                    result = ''.join(re.findall(r'[A-Za-z]', row[0]))
                    row.insert(1, train_info_analyse(result))
                print(row[i].ljust(11, ' ') + "   ", end="")
            print()
        f.close()
    return True


# 查询列车信息
def train_query():
    train_flag = False
    while True:
        time_display('time')
        print(" 查询列车信息".rjust(27, '^'))
        print(" 要查询车次信息请直接输入所要查询的车次或地点 \n 输入'查询'或'all'或'a'输出所有列车信息 \n 输入'退出'或'quit'或'q'退出查询")
        in_temp = input(" 请输入想要的操作:".rjust(30, '^')).strip()
        if "查询" == in_temp or "all" == in_temp or "a" == in_temp:
            print(" 输出列车信息：".rjust(28, '^'))
            # 读取列车信息数据库
            train_info_output()
        elif "退出" == in_temp or "quit" == in_temp or "q" == in_temp:
            break
        else:
            # 查询输入的列车信息
            with open('Data/trainInfomation.csv', 'r', encoding='gbk') as f:  # trainInfomation.csv 为当前目录文件
                train_csv = csv.reader(f)
                # 输出表头
                for i in range(len(train_info_display)):
                    print(train_info_display[i].ljust(11, ' ') + "  ", end="")
                print()
                # 输出列车信息
                for row in train_csv:  # 文件遍历 -- 以行的形式
                    if in_temp in row:
                        for i in range(len(row) + 1):  # 行遍历 -- 以每一个数据
                            if 0 == i:
                                result = ''.join(re.findall(r'[A-Za-z]', row[0]))
                                row.insert(1, train_info_analyse(result))
                            print(row[i].ljust(11, ' ') + "   ", end="")
                        print()
                        train_flag = True
                    else:
                        train_flag = False
            if not train_flag:
                print("此列车信息未找到")
    return True
