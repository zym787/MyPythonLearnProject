# trainTicketSalesSystem.py
# 🐂小黄牛 火车票售票系统 V1.0
# 管理员账户：root
#      密码：0

# import os
# import sys
# sys.path.append('C:/Users/zym78/PycharmProjects/pythonProject/MyCode/item_TrainTicketSalesSystem/Code)
# sys.path.append(os.path.abspath(os.path.join(os.getcwd(), "/MyCode/item_TrainTicketSalesSystem/Code/")))
# print(os.path.abspath(r"/MyCode/item_TrainTicketSalesSystem/trainTicketSalesSystem.py"))
# 就是将你上层的文件夹添加到工作路径，当然包括文件夹下。
# 另外，这三行要放到你要导入的模块的前面


from admin import *
from user import *


# 界面设计
def main_menu(opt_strs):
    time_display('all')
    print("+" + "这是分割线".center(98, "-") + "+")
    while True:
        opt_str = opt_menu(opt_strs[0])
        # 1 查询列车信息
        if opt_str == '1':
            train_query()
        # 2 管理员登录
        elif opt_str == '2':
            if admin_logon():
                admin_menu(opt_strs[1])
        # 3 用户登录
        elif opt_str == '3':
            if user_logon():
                user_menu(opt_strs[2])
        # 4 用户注册
        elif opt_str == '4':
            if user_register():
                print("注册成功！现在可以登录啦~")
            else:
                print("注册失败！！")
        # 5 退出
        elif opt_str == '5':
            print("退出...")
            return True
        else:
            print("输入错误，请重新输入!!!")


# 多国语言欢迎界面
def welcome_menu():
    screen_width = 100
    text_width = 92
    box_width = text_width + 9
    left_margin = (screen_width - box_width) // 2
    l_margin = [1, 1, 1, 1, 1, 1]
    r_margin = [38, 38, 9, 6, 29, 1]
    print(' ' * left_margin + '+' + '-' * box_width + '+')
    print(' ' * (left_margin + 2) + '| ' + ' ' * text_width + ' |')
    for i in range(len(welc_array)):
        print(' ' * (left_margin + 2) + '| ' + ' ' * l_margin[i] + welc_array[i] + ' ' * r_margin[i] + ' |')
    print(' ' * (left_margin + 2) + '| ' + ' ' * text_width + ' |')
    print(' ' * left_margin + '+' + '-' * box_width + '+')


system_init()
welcome_menu()
main_menu(opt_strs)  # 显示主页面
