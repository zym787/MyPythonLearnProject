# user.py
from commonFile import *


# 购票
def train_ticket_buy():
    while True:
        time_display('time')
        print(" 购票".rjust(27, '^'))
        print(" 要查询车次信息请直接输入所要查询的车次或地点 \n 输入'查询'或'all'或'a'输出所有列车信息 \n"
              " 输入'退出'或'quit'或'q'退出购买 \n 要购买车票，请输入'购买'或'buy'或'b'进入购票模式")
        in_temp = input(" 请输入想要的操作:".rjust(30, '^')).strip()
        if "查询" == in_temp or "all" == in_temp or "a" == in_temp:
            print(" 输出列车信息：".rjust(28, '^'))
            # 读取列车信息数据库
            train_info_output()
        elif "退出" == in_temp or "quit" == in_temp or "q" == in_temp:
            break
        elif "购票" == in_temp or "buy" == in_temp or "b" == in_temp:
            # 购票模式
            print("购票模式")
            time_display(time)
            train_info_output()
            in_train = input(" 请输入想要购买的车次:".rjust(30, '^')).strip()
            with open('Data/trainInfomation.csv', 'r+', encoding='gbk') as f:  # trainInfomation.csv 为Data目录下的文件
                train_csv = csv.reader(f)
                for row in train_csv:  # 文件遍历 -- 以行的形式
                    if in_train in row:
                        buy_flag = True
                        # 找到待查车次并保存相关信息
                        train = in_train
                        price = float(row[3])
                        number = int(row[4])
                        for i in range(len(row) + 1):  # 行遍历 -- 以每一个数据
                            if 0 == i:
                                result = ''.join(re.findall(r'[A-Za-z]', row[0]))
                                row.insert(1, train_info_analyse(result))
                            print(row[i].ljust(11, ' ') + "   ", end="")
                        print()
                        print(" 请确认列车信息，确认无误后输入'yes'购买，输入其他字符将取消购买")
                        print(" 车票金额为:" + str(price) + " 票数为:" + str(number) )
                        in_confirm = input(" 请输入想要的操作:".rjust(30, '^')).strip()
                        if "yes" == in_confirm:
                            money = int(input(" 请输入支付的金额:".rjust(30, '^')))
                            if money >= price:
                                # 票数递减
                                print(" 购买成功！欢迎前来乘车")
                                if money > price:
                                    print(" 钱多了，找您" + str(money - price) + "元")
                                    return True
                                else:
                                    print(" 钱刚好，找您0元")
                                    return True
                            else:
                                print(" 钱给少了！购票失败")
                                return False
                        else:
                            break
                    else:
                        buy_flag = False
            if not buy_flag:
                print(" 输入车次未找到，请重新输入")

        else:
            with open('Data/trainInfomation.csv', 'r+', encoding='gbk') as f:  # trainInfomation.csv 为Data目录下的文件
                train_csv = csv.reader(f)
                # 查询车次
                # 输出表头
                for i in range(len(train_info_display)):
                    print(train_info_display[i].ljust(11, ' ') + "  ", end="")
                print()
                # 输出列车信息
                for row in train_csv:  # 文件遍历 -- 以行的形式
                    if in_temp in row:  # 找到输入的车次
                        # 添加输出信息 插入一列车次解析
                        for i in range(len(row) + 1):  # 行遍历 -- 以每一个数据
                            if 0 == i:
                                result = ''.join(re.findall(r'[A-Za-z]', row[0]))
                                row.insert(1, train_info_analyse(result))
                            print(row[i].ljust(11, ' ') + "   ", end="")
                        print()
                        train_flag = True
                        break
                    else:
                        train_flag = False
            if not train_flag:
                print("此列车信息未找到，请重试")
    return True


# 退票
def train_ticket_ref():
    print("退票")
    return True


# 用户界面
def user_menu(opt_strs):
    time_display('time')
    while True:
        opt_str = opt_menu(opt_strs)
        # 1 查询列车信息
        if opt_str == '1':
            train_query()
        # 2 购票
        elif opt_str == '2':
            train_ticket_buy()
        # 3 退票
        elif opt_str == '3':
            train_ticket_ref()
        # 4 退出
        elif opt_str == '4':
            print(" 退出...".rjust(26, '~'))
            return True
        else:
            print(" 输入错误，请重新输入!!!".rjust(34, '~'))


# 用户注册
def user_register():
    time_display('time')
    register_flag = False  # 用户注册标志
    # 读取用户数据
    with open('Data/userDataBase.csv', 'r', encoding='gbk') as f:  # userDataBase.csv 为当前目录文件
        user = f.readlines()
    if captcha():
        in_name = input(" 请输入用户名：".rjust(28, '~'))
        for cache in user:  # 遍历整个用户数据库，检测用户名是否存在
            cache = cache.split()
            if in_name == cache[0]:
                print("用户已存在！")
                register_flag = True
                return False
            else:
                register_flag = False
                continue
        if not register_flag:  # 用户不存在，为新用户-->允许注册
            # 两次输入用户密码，防止遗忘
            in_passwd_before = input(" 请输入你设置6位字母数字组合的密码：".rjust(39, '~'))
            in_passwd_after = input(" 请再输入一次密码：".rjust(30, '~'))
            # print("注册成功")
            if in_passwd_after == in_passwd_before:
                in_passwd = in_passwd_after
                # 对用户注册的密码进行限制，以增强密码强度
                if not in_passwd.isdigit():
                    if not in_passwd.isalpha():
                        if in_passwd.isalnum() and len(in_passwd) == 6:
                            with open('Data/userDataBase.csv', 'a', encoding='gbk') as f:  # userDataBase.csv 为当前目录用户数据文件
                                f.write(in_name + ',' + in_passwd + '\n')
                                # 注册成功满足的要求：
                                # 1.用户为新用户
                                # 2.密码两次输入正确，且为6位字母数字组合的密码
                                f.close()
                            return True
                        else:
                            print("密码不符合6位字母数字组合的要求，请重试！")
                            return False
                    else:
                        print("密码不符合6位字母数字组合的要求，请重试！")
                        return False
                else:
                    print("密码不符合6位字母数字组合的要求，请重试！")
                    return False
            else:
                print(" 两次输入的密码不一致！请重新输入".rjust(37, '~'))
                return False
        else:
            return False
    else:
        return False


# 用户登录
def user_logon():
    n = 3  # 密码最多可输错的次数
    if captcha():  # 通过验证码认证
        j = 0  # 重置密码输入计数器
        print("Caution!密码位数为6位！连续登录失败超过{}次，账号将被锁定登录！若被锁定请联系管理员".format(n))
        # 输入用户名
        in_name = input(" 请输入用户名:".rjust(28, '~')).strip()
        # 验证用户名&密码
        if 0 != len(in_name):
            with open('Data/userBlacklist.csv', 'r', encoding='gbk') as f:  # userBlacklist.csv 为当前目录文件
                # black_user = f.read()
                black_user_csv = csv.reader(f)
                for black_user_row in black_user_csv:
                    if in_name in black_user_row:
                        print(" 该用户已锁定，请联系管理员".rjust(34, '~'))
                        return False
            with open('Data/userDataBase.csv', 'r', encoding='gbk') as f:  # userDataBase.csv 为当前目录文件
                user_csv = csv.reader(f)
                for user_row in user_csv:
                    if user_row[0] == in_name:  # 匹配到用户名的列表，对应该列表的另一个元素为密码
                        for i in range(n):  # n次密码输入机会
                            j += 1
                            in_passwd = input(" 请输入6位密码:".rjust(29, '~')).strip()
                            if 6 == len(in_passwd):  # 密码限制条件：6位密码
                                if in_passwd == user_row[1]:  # 密码匹配
                                    print(" 登陆成功，欢迎 {} 回家！".format(in_name).rjust(31, '~'))
                                    return True
                                else:
                                    if 0 != (n - j):
                                        print("用户名或者密码错误！还有{}次账户将被锁定!".format(n - j))
                            else:
                                print(" 密码位数不对！请输入6位密码！！!".rjust(38, '~'))
                        # n次密码输入机会用完且用户名与密码不对应将锁定用户。实现方法：是将其用户名写入一个文件中保存
                        else:
                            # with open('Data/userBlacklist.csv', 'r', encoding='gbk') as fp:  # userBlacklist.csv 为当前目录文件
                            #     black_user_csv = csv.reader(fp)
                            #     for black_user_row in black_user_csv:
                            #         if in_name not in black_user_row[0]:  # 遍历用户黑名单
                            with open('Data/userBlacklist.csv', mode='a', encoding='gbk') as fp:
                                fp.write(in_name + '\n')
                                fp.close()
                            print("连续输入用户名或密码错误超过{}次，账户已被锁定登录！请联系管理员".format(n))
                            return False
    else:
        return False
