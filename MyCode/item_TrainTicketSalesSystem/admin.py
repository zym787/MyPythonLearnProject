# admin.py
from commonFile import *
import re
import csv


# 添加列车信息
def train_info_add():
    write_flag = False
    with open('Data/trainInfomation.csv', mode='r+', encoding='gbk') as f:
        train = f.readlines()
        train_csv = csv.reader(f)
        in_info_str = str(input("请输入" + train_info_added[0] + ":"))
        for row in train:  # 遍历文件 车次查重
            if in_info_str in row:
                write_flag = False
                break
            else:
                # 置可添加车次标志
                write_flag = True
        # 判断是否可以添加该车次
        if write_flag:  # 可添加车次标志
            write_flag = False
            f.write(in_info_str[:10])
            for i in range(len(train_info_added) - 1):  # 输入列车信息
                in_info_str = str(input("请输入" + train_info_added[i + 1] + ":"))
                f.write("," + in_info_str[:10])
            print("车次不存在，已添加")
            f.write("\n")
            f.close()
        else:
            print("该车次已存在！请检查后重新输入")
        with open('Data/trainInfomation.csv', mode='r+', encoding='gbk') as f:
            train_csv = csv.reader(f)
            for row in train_csv:  # 文件遍历 -- 以行的形式
                for i in range(len(row) + 1):  # 行遍历 -- 以每一个数据
                    if 0 == i:
                        result = ''.join(re.findall(r'[A-Za-z]', row[0]))
                        row.insert(1, train_info_analyse(result))
                    print(row[i].ljust(11, ' ') + "   ", end="")
                print()
    return True


# 删除列车信息
def train_info_del():
    train_flag = False
    while True:
        time_display('time')
        # 输出列车信息
        print(" 输出列车信息：".rjust(28, '^'))
        # 读取列车信息数据库
        with open('Data/trainInfomation.csv', 'r+', encoding='gbk') as f:  # trainInfomation.csv 为Data目录下的文件
            train_csv = csv.reader(f)
            # 输出表头
            for i in range(len(train_info_display)):
                print(train_info_display[i].ljust(11, ' ') + "  ", end="")
            print()
            # 输出列车信息
            for row in train_csv:  # 文件遍历 -- 以行的形式
                for i in range(len(row) + 1):  # 行遍历 -- 以每一个数据
                    if 0 == i:
                        result = ''.join(re.findall(r'[A-Za-z]', row[0]))
                        row.insert(1, train_info_analyse(result))
                    print(row[i].ljust(11, ' ') + "   ", end="")
                print()
        print(" 删除列车信息".rjust(27, '^'))
        print(" 要删除车次信息请直接输入所要删除的车次 \n 输入'退出'或'quit'或'q'退出查询")
        in_temp = input(" 请输入想要的操作:".rjust(30, '^')).strip()
        if "退出" == in_temp or "quit" == in_temp or "q" == in_temp:
            break
        else:
            # 检测输入车次是否正确
            if 'K' == in_temp[0] or 'D' == in_temp[0] or 'G' == in_temp[0] or 'T' == in_temp[0] or \
                    'k' == in_temp[0] or 'd' == in_temp[0] or 'g' == in_temp[0] or 't' == in_temp[0]:
                with open('Data/trainInfomation.csv', mode='r+', encoding='gbk') as f:
                    train = f.readlines()
                    train_row_temp = "" # 重写文件缓存
                    train_flag = False  # 重写文件标志
                    for train_row in train:  # 遍历文件 车次查重
                        if in_temp in train_row:
                            train_flag = False
                        else:
                            # 置可重写文件标志
                            train_flag = True
                            train_row_temp += train_row
                    # 判断是否可以重写文件
                    if train_flag:  # 可重写文件标志
                        train_flag = False
                        print("是否删除！！！此操作无法撤销！ \n 请输入'y'或'yes'确认删除")
                        confirm = input(" 请输入想要的操作:".rjust(30, '^')).strip()
                        if "y" == confirm or "yes" == confirm:
                            # 确认删除车次
                            with open('Data/trainInfomation.csv', 'w+', encoding='gbk') as w:
                                w.truncate()
                                w.write(train_row_temp)
                                w.close()
                            print("删除成功！")
                            # 输出列车信息
                            with open('Data/trainInfomation.csv', 'r',
                                      encoding='gbk') as f:  # trainInfomation.csv 为Data目录下的文件
                                train_csv = csv.reader(f)
                                # 输出表头
                                for i in range(len(train_info_display)):
                                    print(train_info_display[i].ljust(11, ' ') + "  ", end="")
                                print()
                                # 输出列车信息
                                for row in train_csv:  # 文件遍历 -- 以行的形式
                                    for i in range(len(row) + 1):  # 行遍历 -- 以每一个数据
                                        if 0 == i:
                                            result = ''.join(re.findall(r'[A-Za-z]', row[0]))
                                            row.insert(1, train_info_analyse(result))
                                        print(row[i].ljust(11, ' ') + "   ", end="")
                                    print()
                        else:
                            print("取消删除")
                    else:
                        print("此列车未找到，无法删除")
            else:
                print("输入的不是车次，请检查！")
    return True


# 修改列车信息
def train_info_change():
    print("修改列车信息")
    return True


# 用户解锁
def user_unlock():
    with open(r'Data/userBlacklist.csv', 'r', encoding='gbk') as f:
        l1 = f.readlines()
    print(" 被锁定的用户:".rjust(28, '~'), l1)
    im_name = input(" 请输入你要解锁的用户名:".rjust(33, '~'))
    with open(r'Data/userBlacklist.csv', 'r', encoding='gbk') as f:
        l1 = f.readlines()
    with open(r'Data/userBlacklist.csv', 'w', encoding='gbk') as w:
        for l in l1:
            if im_name not in l:
                w.write(l)
            else:
                w.write("")
    return True


# 管理员登录
def admin_logon():
    time_display('time')
    if captcha():
        in_name = input(" 请管理员输入用户名：".rjust(31, '~'))
        in_passwd = input(" 请管理员输入密码：".rjust(30, '~'))
        if in_name == "root" and in_passwd == "0":
            print(" 登录成功，欢迎帅气的管理员".rjust(34, '~'))
            return True
        else:
            print(" 用户名或者密码错误！请重新输入".rjust(36, '~'))
            return False
    else:
        return False


# 管理员界面
def admin_menu(opt_strs):
    time_display('time')
    while True:
        opt_str = opt_menu(opt_strs)
        # 1 查询列车信息
        if opt_str == '1':
            train_query()
        # 2 添加列车信息
        elif opt_str == '2':
            train_info_add()
        # 3 删除列车信息
        elif opt_str == '3':
            train_info_del()
        # 4 修改列车信息
        elif opt_str == '4':
            train_info_change()
        # 5 用户解锁
        elif opt_str == '5':
            user_unlock()
        # 6 退出
        elif opt_str == '6':
            print("退出...".rjust(26, '~'))
            return True
        else:
            print("输入错误，请重新输入!!!".rjust(34, '~'))
