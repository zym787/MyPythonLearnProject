opt_strs = [
    ["查询列车信息", "管理员登录 ", "用户登录  ", "用户注册  ", "退出系统  "],
    ["查询列车信息", "添加列车信息", "删除列车信息", "修改列车信息", "用户解锁  ", "返回上一级 "],
    ["查询列车信息", "用户购票  ", "用户退票  ", "返回上一级 "]
]
# 多语种欢迎语
welc_array = [
    "中文简体:         欢迎使用--- 🐂小黄牛 ---火车票售票系统 V1.0",
    "中文繁體:         歡迎使用--- 🐂小黃牛 ---火車票售票系統 V1.0",
    "русск:           Добро пожаловать--- 🐂Желторотый ---Система билетов на поезд V1.0",
    "English:         Welcome to use--- 🐂Little Scalper ---Train ticket sales system V1.0",
    "日本語:           ご利用歓迎--- 🐂小牡牛 ---列車の切符の販売システムV1.0",
    "Das ist Deutsch: Willkommen an bord--- 🐂Schwarzblut ---Reisetickets: automatensystem V1.0"
]
# 选项函数
def opt_menu(opt_strs):
    for i in range(len(opt_strs)):
        print("{} {}：{} {}".format("", i + 1, opt_strs[i], "").center(50, '*'))
    opt_str = input("{} 请输入操作编号 1~{}：".format("", len(opt_strs)).rjust(33, '*'))
    return opt_str
# 多国语言欢迎界面
def welc_menu():
    screen_width = 100
    text_width = 92
    box_width = text_width + 9
    left_margin = (screen_width - box_width) // 2
    l_margin = [1, 1, 1, 1, 1, 1]
    r_margin = [38, 38, 9, 6, 29, 1]
    print(' ' * left_margin + '+' + '-' * box_width + '+')
    print(' ' * (left_margin + 2) + '| ' + ' ' * text_width + ' |')
    for i in range(len(welc_array)):
        print(' ' * (left_margin + 2) + '| ' + ' ' * l_margin[i] + welc_array[i] + ' ' * r_margin[i] + ' |')
    print(' ' * (left_margin + 2) + '| ' + ' ' * text_width + ' |')
    print(' ' * left_margin + '+' + '-' * box_width + '+')

opt_menu(opt_strs[0])
# opt_menu(opt_strs[1])
# opt_menu(opt_strs[2])

# n = 3
# j = 1
# check = 1000
# print(" 输入错误，请重新输入!!!".rjust(34, '*'))
# print(" 验证码错误！".rjust(27,'*'))
# input(" 请输入验证码({})：".format(check).rjust(34, '*'))
#
# print(" 退出...".rjust(26, '*'))
# print(" 输入错误，请重新输入!!!".rjust(34, '*'))
#
# input(" 请输入用户名：".rjust(28, '*'))
# input(" 请输入你设置6位字母数字组合的密码：".rjust(39, '*'))
# input(" 请再输入一次密码：".rjust(30, '*'))
# print(" 两次输入的密码不一致！请重新输入".rjust(37, '*'))
# print(" Caution!密码位数为6位！连续登录失败超过{}次，账号将被锁定登录！若被锁定请联系管理员".format(n))
# input(" 请输入用户名:".rjust(28, '*')).strip()
# print(" 该用户已锁定，请联系管理员".rjust(34, '*'))

input(" 请输入6位密码:".rjust(29, '*')).strip()
print(" 登陆成功，欢迎回家！".rjust(31, '*'))
# print(" 用户名或者密码错误！还有{}次账户将被锁定!".format(n - j))
print(" 密码位数不对！请输入6位密码！！!".rjust(38, '*'))
# print(" 连续输入用户名或密码错误超过{}次，账户已被锁定登录！请联系管理员".format(n))
print(" 用户不存在，请注册！".rjust(31, '*'))

with open(r'userBlacklist.csv', 'r', encoding='gbk') as f:
    l1 = f.readlines()
print(" 被锁定的用户:".rjust(28, '*'), l1)
input(" 请输入你要解锁的用户名:".rjust(33, '*'))
input(" 请管理员输入用户名：".rjust(31, '*'))
input(" 请管理员输入密码：".rjust(30, '*'))
print(" 登录成功，欢迎帅气的管理员".rjust(34, '*'))
print(" 用户名或者密码错误！请重新输入".rjust(36, '*'))
print(" 伙计，试着输入一些数字吧".rjust(32, '~'))