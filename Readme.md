# trainTicketSalesSystem.py
# 🐂小黄牛 火车票售票系统 V1.0 newfeatures


### 文件树
```
卷 Window 的文件夹 PATH 列表
卷序列号为 678B-9754
C:.
├─.idea
│  └─inspectionProfiles
├─MyCode
│  ├─item_TrainTicketSalesSystem
│  ├─LoginVerificationSystem(ForTest)
│  ├─menuDisplay(ForTest)
│  ├─theFilmTicketSalesSystem(ForTests)
│  └─《Python程序设计》期末考核
│      └─学号 姓名
│          └─源码
└─venv
    ├─Include
    │  ├─openssl
    │  ├─p11-kit-1
    │  │  └─p11-kit
    │  ├─pdcurses
    │  ├─python2.7
    │  ├─tcl8.6
    │  │  └─tcl-private
    │  │      ├─generic
    │  │      └─win
    │  ├─tk8.6
    │  │  └─tk-private
    │  │      ├─generic
    │  │      │  └─ttk
    │  │      └─win
    │  └─X11
    ├─Lib
    │  ├─config
    │  ├─lib-dynload
    │  ├─python2.7
    │  │  ├─encodings
    │  │  └─lib-dynload
    │  └─site-packages
    └─Scripts

```